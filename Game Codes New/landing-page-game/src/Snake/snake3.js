/* ==============================================================================================
 * Variables
 * ==============================================================================================*/
 
	/*Snake Variables*/
	var snake;
	var snakeLength;
	var snakeSize;
	var snakeDirection;
	
	/*Food Variables*/
	var food; 
	
	/*Canvas Variables*/
	var context;
	var screenWidth;
	var screenHeight;

	/*Game Variables*/
	var gameState;
	var gameOverMenu;
	var restartButton;
	var playHUD;
	var scoreboard;


/* ==============================================================================================
 * Game Execution
 * ==============================================================================================*/

	/*Execute Game Code - (setInterval = time interval in milliseconds (Speed for moving the snake)*/
	gameInitialize();
	snakeInitialize();
	foodInitialize();
	setInterval(gameLoop, 1000 / 30); 


/* ==============================================================================================
 * Game Methods
 * ==============================================================================================*/
 
	function gameInitialize() {
		var canvas = document.getElementById('snakeGame');  //Assign variable from HTML Element//
		context = canvas.getContext("2d");  //Context of the Canvas//
	
		//Setting Screen Size//
		screenWidth = window.innerWidth;
		screenHeight = window.innerHeight;
	
		//Setting Canvas Size//
		canvas.width = screenWidth;
		canvas.height = screenHeight;
	
		//Event Listener - read keyboards input to move snake//
		document.addEventListener("keydown", keyboardHandler);
	
		gameOverMenu =  document.getElementById("gameOver");
		centerMenuPosition(gameOverMenu);
	
		restartButton = document.getElementById("restartButton");
		restartButton.addEventListener("click", gameRestart);
		
		playHUD = document.getElementById("playHUD");
		scoreboard = document.getElementById("scoreboard");
	
		setState("PLAY");
	}

	function gameLoop() {
		gameDraw();
		drawScoreboard();
		if (gameState == "PLAY") {
			snakeUpdate();
			snakeDraw();
			foodDraw();
		}
	}

	function gameDraw() {
		context.fillStyle = "rgb(143, 130,114)"; //Set background color//
		context.fillRect(0, 0, screenWidth, screenHeight); //Set Background for entire screen//
	}

	function gameRestart() {
		snakeInitialize();
		foodInitialize();
		hideMenu(gameOverMenu);
		setState("PLAY");
	}
 
/* ==============================================================================================
 * Snake Methods
 * ==============================================================================================*/
 
	function snakeInitialize() {
	//Setting up snake functions//
		snake = []; //Snake Array//
		snakeLength = 1; //Snake Length//
		snakeSize = 20; //Snake Size//
		snakeDirection = "right";
	
			//Postion for each part of snake//
				for(var index = snakeLength - 1; index >= 0; index--) {
					snake.push( {
						x: index,
						y: 0
					});
				}
            }


	function snakeDraw() {
	//Draw Snake//
		for(var index = 0; index < snake.length; index++) { //Draw Snake using Loops// 
			context.fillStyle = "white"; //Snake Color//	
			context.fillRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize); //Snake made of small rectangles//
		}	
	}

	function snakeUpdate() {
	//Update the postion of the snake//
		var snakeHeadX = snake[0].x;
		var snakeHeadY = snake[0].y; {
			if(snakeDiretion == "down") {
				snakeHeadY++;
				}
			else if (snakeDirection == "right"){
				snakeHeadX++;
			}
			else if (snakeDirection == "left"){
				snakeHeadX--;
			}
			else (snakeDirection == "up"){
				snakeHeadY--;
			}
		}
	
		//Snake interaction with Food and walls//
		checkFoodCollisions(snakeHeadX, snakeHeadY);
		checkWallCollisions(snakeHeadX, snakeHeadY);
		checkSnakeCollisions(snakeHeadX, snakeHeadY);
	
		var snakeTail = snake.pop();
		snakeTail.x = snakeHeadX;
		snakeTail.y = snakeHeadY;
		snake.unshift(snakeTail);
	}

 
/* ==============================================================================================
 * Food methods
 * ==============================================================================================*/

	//initializes Food on Screen position//
	function foodInitialize() {
		food = {
			x: 0,
			y: 0
			};
		setfoodPosition();
	}

	//Prints Food on Screen//
	function foodDraw() {
		context.fillStyle = "White";
		context.fillRect(food.x * snakeSize, food.y * snakeSize, snakeSize, snakeSize);
	}

	//generate random numbers for food position// 
	function setFoodPosition() {
		var randomX = Math.floor(Math.random() * screenWidth);
		var randomY = Math.floor(Math.random() * screenHeight);
	
	//using random number generated to set food postion//
	food.x = Math.floor(randomX / snakeSize);
	food.y = Math.floor(randomY / snakeSize);
	}


/* ==============================================================================================
 * Keyboard Handling - Input
 * ==============================================================================================*/

	function keyboardHandler (event) {
		console.log(event);
	
		if(event.keyCode == "39" && snakeDirection != "left"){
			snakeDirection == "right";
			}
		else if(event.keyCode == "37" && snakeDirection != "right"){
			snakeDirection == "left";
			}
		else if(event.keyCode == "40" && snakeDirection != "up"){
			snakeDirection =="down";
			}
		else(event.keyCode == "38" && snakeDirection != "down"){
			snakeDirection =="up";
			}
		}


/* ==============================================================================================
 * Collisions
 * ==============================================================================================*/
 
	function checkFoodCollisions (snakeHeadX, snakeHeadY) {
		if (snakeHeadX == food.x && snakeHeadY == food.y) {
			snake.push({
				x: 0,
				y: 0
			});
			snakeLength++;
		}
	}

	function checkWallCollisions(snakeHeadX, snakeHeadY) {
		if (snakeHeadX * snakeSize >= screenwidth || snakeHeadX * snakeSize < 0) {
			setState("GAME OVER"); 
		}
	}

	function checkSnakeCollisions(snakeHeadX, snakeHeadY) {
		for(var index = 1; index < snake.length; index ++) {
			if(snakeHeadX == snake[index].x  && snakeHeadY == snake[index].y) {
				setState("GAME OVER");
				return;
			}
		}	
	}


/* ==============================================================================================
 * Game State Handling
 * ==============================================================================================*/

	//Game State Handling//
	function setState(state) {
		gameState = state;
		showMenu(state);
	}
 

/* ==============================================================================================
 * Menu Methods
 * ==============================================================================================*/

	//Menu Functions//
	function displayMenu(menu) {
		menu.style.visibility = "visible";
	}

	function hideMenu(menu)  {
		menu.style.visibility = "hidden";
	}

	function showMenu(state) {
		if (state == "GAME OVER") {
			displayMenu(gameOverMenu);
		}
		else if (state == "PLAY")  {
			displayMenu(playHUD);
		}
	}

	function centerMenuPosition(menu) {
		menu.style.top = (screenHeight / 2) - (menu.offsetHeight / 2) + "px";
		menu.style.left = (screenWidth / 2) - (menu.offsetWidth / 2) + "px";
	}
	
	function drawScoreboard() {
		scoreboard.innerHTML = "Score: " + snakeLength;
	}