var BoxOpened = "";
var ImgOpened = "";
var Counter = 0;
var ImgFound = 0;
var btnNext = document.getElementById("nextGame");
var Source = "#boxcard";

// where all the pictures comes from  this is the original picture size must be 100px x 100px
var ImgSource = [
  "https://i.ibb.co/Hn1jNZG/Goal5.png",
  "https://i.ibb.co/tzLXb5B/Goal4.png",
  "https://i.ibb.co/27rnVJd/Goal3.png",
  "https://i.ibb.co/T1bTgt2/Goal2.png",
  "https://i.ibb.co/0V1S1Pp/Goal1.png",
  "https://i.ibb.co/fGfNcJ9/Value1.png",  
  "https://i.ibb.co/92PtvcC/Value2.png",
  "https://i.ibb.co/86FmVsT/Value3.png",  
  "https://i.ibb.co/Nn2FDR2/Value4.png",
  "https://i.ibb.co/nQTnBTm/Value5.png",
];


function RandomFunction(MaxValue, MinValue) {
		return Math.round(Math.random() * (MaxValue - MinValue) + MinValue);
	}
	
function ShuffleImages() {
	var ImgAll = $(Source).children();
	var ImgThis = $(Source + " div:first-child");
	var ImgArr = new Array();

	for (var i = 0; i < ImgAll.length; i++) {
		ImgArr[i] = $("#" + ImgThis.attr("id") + " img").attr("src");
		ImgThis = ImgThis.next();
	}
	
		ImgThis = $(Source + " div:first-child");
	
	for (var z = 0; z < ImgAll.length; z++) {
	var RandomNumber = RandomFunction(0, ImgArr.length - 1);

		$("#" + ImgThis.attr("id") + " img").attr("src", ImgArr[RandomNumber]);
		ImgArr.splice(RandomNumber, 1);
		ImgThis = ImgThis.next();
	}
}

function NextGame() {
// if quitQuiz button clicked
btnNext.onclick = ()=>{
    window.location.href = "../Snake/snake2.html"; //reload the current window
}
}

function ResetGame() {
	ShuffleImages();
	$(Source + " div img").hide();
	$(Source + " div").css("visibility", "visible");
	Counter = 0;
	$("#success").remove();
	$("#counter").html("" + Counter);
	BoxOpened = "";
	ImgOpened = "";
	ImgFound = 0;
	return false;
}

function OpenCard() {
	var id = $(this).attr("id");

	if ($("#" + id + " img").is(":hidden")) {
		$(Source + " div").unbind("click", OpenCard);
	
		$("#" + id + " img").slideDown('fast');

		if (ImgOpened == "") {
			BoxOpened = id;
			ImgOpened = $("#" + id + " img").attr("src");
			setTimeout(function() {
				$(Source + " div").bind("click", OpenCard)
			}, 300);
		} else {
			CurrentOpened = $("#" + id + " img").attr("src");
			if (ImgOpened != CurrentOpened) {
				setTimeout(function() {
					$("#" + id + " img").slideUp('fast');
					$("#" + BoxOpened + " img").slideUp('fast');
					BoxOpened = "";
					ImgOpened = "";
				}, 400);
			} else {
				$("#" + id + " img").parent().css("visibility", "hidden");
				$("#" + BoxOpened + " img").parent().css("visibility", "hidden");
				ImgFound++;
        //this will add the score
        Counter++;
		    $("#counter").html("" + Counter);
				BoxOpened = "";
				ImgOpened = "";
			}
			setTimeout(function() {
				$(Source + " div").bind("click", OpenCard)
			}, 400);
		}

		if (ImgFound == ImgSource.length) {
			$("#counter").prepend('<span id="success">You Found All /span>');
			nextGame.style.visibility = 'visible'; 
		}
	}
}

$(function() {

for (var y = 1; y < 3 ; y++) {
	$.each(ImgSource, function(i, val) {
		$(Source).append("<div id=card" + y + i + "><img src=" + val + " />");
	});
}
	$(Source + " div").click(OpenCard);
    nextGame.style.visibility = 'hidden'; 
	ShuffleImages();
});
