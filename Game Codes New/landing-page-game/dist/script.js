// constants
const details = document.querySelector('.login_form'),
      form = details.querySelectorAll('.form'),
      startBtn = form[0].querySelector('button[type="submit"]');

//Validtion Code For Inputs
var name = document.forms['form']['player_Name'];
var email = document.forms['form']['email'];
var password = document.forms['form']['password'];
var level = document.forms['form']['level'];


var email_error = document.getElementById('email_error');
var pass_error = document.getElementById('pass_error');

email.addEventListener('textInput', email_Verify);
password.addEventListener('textInput', pass_Verify);

function validated(){

	if (email.value.length < 9) {
		email.style.border = "1px solid red";
		email_error.style.display = "block";
		email.focus();
		return false;
	}
	if (password.value.length < 6) {
		password.style.border = "1px solid red";
		pass_error.style.display = "block";
		password.focus();
		return false;
	}

}
function email_Verify(){
	if (email.value.length >= 8) {
		email.style.border = "1px solid silver";
		email_error.style.display = "none";
		return true;
	}
}
function pass_Verify(){
	if (password.value.length >= 5) {
		password.style.border = "1px solid silver";
		pass_error.style.display = "none";
		return true;
	}
}
// this will get all information from the form
// it will go through all input fields and collect the data
function getPlayerDetails(e){
  e.preventDefault();
  var formData = new FormData(form[0]);
  alert(formData.get('name') + '-' + formData.get('email') + '-' + formData.get('levels'));
}

// event handler

document.addEventListener('DOMContentLoaded', function(){
  
  startBtn.addEventListener('click',getPlayerDetails,fasle);
  
  
}, false);