
function(level){
	switch (level)
	
		case 1:
		
		function startTimer(duration, display) {
		var timer = duration, minutes, seconds;
			setInterval(function () {
				minutes = parseInt(timer / 60, 10)
				seconds = parseInt(timer % 60, 10);

				minutes = minutes < 10 ? "0" + minutes : minutes;
				seconds = seconds < 10 ? "0" + seconds : seconds;

				display.textContent = minutes + ":" + seconds;
			
			// this is where game over must be
				if (--timer < 0) {
					timer = duration;
				}
			}, 1000);
		}
//MIGHT HAVE TO SET IT IN INDEX
		window.onload = function () {
			var twentyMinutes = 60 * 20,
				display = document.querySelector('#time');
			startTimer(twentyMinutes, display);
		};
		break;
		
		case 2:
		
		function startTimer(duration, display) {
		var timer = duration, minutes, seconds;
			setInterval(function () {
				minutes = parseInt(timer / 60, 10)
				seconds = parseInt(timer % 60, 10);

				minutes = minutes < 10 ? "0" + minutes : minutes;
				seconds = seconds < 10 ? "0" + seconds : seconds;

				display.textContent = minutes + ":" + seconds;
			
			// this is where game over must be
				if (--timer < 0) {
					timer = duration;
				}
			}, 1000);
		}
//MIGHT HAVE TO SET IT IN INDEX
		window.onload = function () {
			var tenyMinutes = 60 * 10,
				display = document.querySelector('#time');
			startTimer(tenMinutes, display);
		};		
		break;
		
		case 3:
		
		function startTimer(duration, display) {
		var timer = duration, minutes, seconds;
			setInterval(function () {
				minutes = parseInt(timer / 60, 10)
				seconds = parseInt(timer % 60, 10);

				minutes = minutes < 10 ? "0" + minutes : minutes;
				seconds = seconds < 10 ? "0" + seconds : seconds;

				display.textContent = minutes + ":" + seconds;
			
			// this is where game over must be
				if (--timer < 0) {
					timer = duration;
				}
			}, 1000);
		}
//MIGHT HAVE TO SET IT IN INDEX
		window.onload = function () {
			var fiveMinutes = 60 * 5,
				display = document.querySelector('#time');
			startTimer(fiveMinutes, display);
		};
		break;
}