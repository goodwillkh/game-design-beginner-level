// constants
const details = document.querySelector('.login_form'),
      form = details.querySelectorAll('.form'),
      startBtn = form[0].querySelector('button[type="submit"]');

//Validtion Code For Inputs
var email = document.forms['form']['email'];
var password = document.forms['form']['password'];


var email_error = document.getElementById('email_error');
var pass_error = document.getElementById('pass_error');

email.addEventListener('textInput', email_Verify);
password.addEventListener('textInput', pass_Verify);

function validated(){
  storePlayer();

	if (email.value.length < 9) {
		email.style.border = "1px solid red";
		email_error.style.display = "block";
		email.focus();
		return false;
	}
	if (password.value.length < 6) {
		password.style.border = "1px solid red";
		pass_error.style.display = "block";
		password.focus();
		return false;
	}

}
function email_Verify(){
	if (email.value.length >= 8) {
		email.style.border = "1px solid silver";
		email_error.style.display = "none";
		return true;
	}
}
function pass_Verify(){
	if (password.value.length >= 5) {
		password.style.border = "1px solid silver";
		pass_error.style.display = "none";
		return true;
	}
}
// this will get all information from the form
// it will go through all input fields and collect the data
function storePlayer(){
//to get the player Name
var pName1 = document.getElementsByName("player_Name")[0].value;
alert(" "+ pName1);

// to get the levels
var lv = document.getElementById("levels");
var plevel = lv.options[lv.selectedIndex].text; // to get the text
var plevVal = lv.options.lv.selectedIndex;  // to get the value

// now let us store it in the local storage of the browser

 window.localStorage.setItem('pName', pName1); //stores the player name in the browser
 window.localStorage.setItem('levelD', plevel); //stores the level name in the browser
 alert( window.localStorage.getItem('levelD'));
}
// this is to test the page
