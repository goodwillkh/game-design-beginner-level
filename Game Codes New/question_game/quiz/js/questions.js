// creating an array and passing the number, questions, options, and answers
let questions = [
    {
    numb: 1,
    question: "When did Namibia gain independence?",
    answer: "1990",
    options: [
      "Hyper Text Preprocessor",
      "Hyper Text Markup Language",
      "Hyper Text Multiple Language",
      "1990"
    ]
  },
    {
    numb: 2,
    question: "Who was the first President of Namibia?",
    answer: "Sam Nujoma",
    options: [
      "Sam Nujoma",
      "Colorful Style Sheet",
      "Computer Style Sheet",
      "Cascading Style Sheet"
    ]
  },
    {
    numb: 3,
    question: "What's the capital city of Namibia?",
    answer: "Windhoek",
    options: [
      "Hypertext Preprocessor",
      "Windhoek",
      "Hypertext Preprogramming",
      "Hometext Preprocessor"
    ]
  },
    {
    numb: 4,
    question: "What's the name of the ocean along Namibia's western border?",
    answer: "Atlantic",
    options: [
      "Stylish Question Language",
      "Stylesheet Query Language",
      "Atlantic",
      "Structured Query Language"
    ]
  },
    {
    numb: 5,
    question: "Who is the current President of Namibia?",
    answer: "Hage Geingob",
    options: [
      "eXtensible Markup Language",
      "eXecutable Multiple Language",
      "Hage Geingob",
      "eXamine Multiple Language"
    ]
  },
  // we can uncomment the below codes and add as many questions as we'd like
  // but we'll have to give the number values and serialize them

  //   {
  //   numb: 6,
  //   question: "Your Question is Here",
  //   answer: "Correct answer of the question is here",
  //   options: [
  //     "Option 1",
  //     "option 2",
  //     "option 3",
  //     "option 4"
  //   ]
  // },
];
